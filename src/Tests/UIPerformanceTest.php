<?php

namespace Drupal\paragraphs_performance\Tests;

use Drupal\paragraphs\Tests\Classic\ParagraphsTestBase;

/**
 * Tests UI paragraphs performance.
 *
 * @group paragraphs_performance_test
 */
class UIPerformanceTest extends ParagraphsTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'image',
  ];

  /**
   * Key value pair with time and marker for nested paragraphs.
   *
   * @var array
   */
  protected $timeStampsNestedParagraphs = [];

  /**
   * Key value pair with time and marker for mixed paragraphs.
   *
   * @var array
   */
  protected $timeStampsMixedParagraphs = [];

  /**
   * Key value pair with time and marker for text only paragraphs.
   *
   * @var array
   */
  protected $timeStampsTextParagraphs = [];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->addParagraphedContentType('paragraphed_test', 'field_paragraphs');
    $this->loginAsAdmin(['edit any paragraphed_test content']);
  }

  /**
   * Creating 100 text paragraphs on one node.
   */
  public function testCreateTextParagraphs() {
    $this->addParagraphsType('text_type');
    static::fieldUIAddNewField('admin/structure/paragraphs_type/text_type', 'text', 'Text', 'text_long', [], []);
    $this->drupalGet('node/add/paragraphed_test');
    $new_stamp = $this->start('Starting adding text only paragraphs');
    $this->timeStampsTextParagraphs[key($new_stamp)] = reset($new_stamp);
    for ($i = 1; $i <= 100; $i++) {
      $this->drupalPostForm(NULL, NULL, t('Add text_type'));
      $new_stamp = $this->start($i . ' paragraph added.');
      $this->timeStampsTextParagraphs[key($new_stamp)] = reset($new_stamp);
    }

    $edit = [
      'title[0][value]' => '100TextParagraphs',
      'field_paragraphs[0][subform][field_text][0][value]' => 'Some text',
    ];

    for ($i = 1; $i <= 100; $i++) {
      $edit += [
        'field_paragraphs[' . $i . '][subform][field_text][0][value]' => 'Some text',
      ];
    }
    $this->drupalPostForm(NULL, $edit, t('Save'));
    $this->assertText('paragraphed_test 100TextParagraphs has been created.');
    $new_stamp = $this->start('Saving paragraphs.');
    $this->timeStampsTextParagraphs[key($new_stamp)] = reset($new_stamp);
    $this->drupalGet('node/1/edit');

    // Change edit mode to closed.
    $this->setParagraphsWidgetMode('paragraphed_test', 'field_paragraphs', 'closed');
    $new_stamp = $this->start('Changing mode to closed.');
    $this->timeStampsTextParagraphs[key($new_stamp)] = reset($new_stamp);
    $this->drupalGet('node/1/edit');
    $this->drupalPostForm(NULL, NULL, 'Save');
    $new_stamp = $this->start('Saving node with closed mode.');
    $this->timeStampsTextParagraphs[key($new_stamp)] = reset($new_stamp);

    // Change edit mode to preview.
    $this->setParagraphsWidgetSettings('paragraphed_test', 'field_paragraphs', ['closed_mode' => 'preview']);
    $new_stamp = $this->start('Changing mode to preview.');
    $this->timeStampsTextParagraphs[key($new_stamp)] = reset($new_stamp);
    $this->drupalGet('node/1/edit');
    $this->drupalPostForm(NULL, NULL, 'Save');
    $new_stamp = $this->start('Saving node with preview mode');
    $this->timeStampsTextParagraphs[key($new_stamp)] = reset($new_stamp);
    $this->calculateSpentTime($this->timeStampsTextParagraphs);
  }

  /**
   * Creating 100 mixed(text, images, account reference) paragraphs on one node.
   */
  public function testCreateMixedParagraphs() {
    $this->addParagraphsType('text_type');
    static::fieldUIAddNewField('admin/structure/paragraphs_type/text_type', 'text', 'Text', 'text_long', [], []);
    $this->addParagraphsType('images_type');
    static::fieldUIAddNewField('admin/structure/paragraphs_type/images_type', 'images', 'Images', 'image', [], []);
    $this->addParagraphsType('user_type');
    static::fieldUIAddNewField('admin/structure/paragraphs_type/user_type', 'user', 'User', 'entity_reference', ['settings[target_type]' => 'user'], []);
    $this->drupalGet('node/add/paragraphed_test');
    $new_stamp = $this->start('Starting adding mixed paragraphs.');
    $this->timeStampsMixedParagraphs[key($new_stamp)] = reset($new_stamp);
    for ($i = 0; $i <= 100; $i++) {
      if ($i % 3 == 0) {
        $this->drupalPostForm(NULL, NULL, t('Add user_type'));
        $new_stamp = $this->start($i . ' user paragraph added.');
        $this->timeStampsMixedParagraphs[key($new_stamp)] = reset($new_stamp);
        continue;
      }

      if ($i % 2 == 0) {
        $this->drupalPostForm(NULL, NULL, t('Add images_type'));
        $new_stamp = $this->start($i . ' image paragraph added.');
        $this->timeStampsMixedParagraphs[key($new_stamp)] = reset($new_stamp);
        continue;
      }

      $this->drupalPostForm(NULL, NULL, t('Add text_type'));
      $new_stamp = $this->start($i . '  text paragraph added.');
      $this->timeStampsMixedParagraphs[key($new_stamp)] = reset($new_stamp);
    }

    $edit = [
      'title[0][value]' => '100MixedParagraphs',
    ];

    for ($i = 0; $i < 100; $i++) {
      if ($i % 3 == 0) {
        $user = $this->admin_user->label() . ' (' . $this->admin_user->id() . ')';
        $edit += [
          'field_paragraphs[' . $i . '][subform][field_user][0][target_id]' => $user,
        ];
        continue;
      }

      if ($i % 2 == 0) {
        $image = $this->drupalGetTestFiles('image')[0];
        $this->drupalPostForm(NULL, [
          'files[field_paragraphs_' . $i . '_subform_field_images_0]' => $image->uri,
        ], t('Upload'));
        $edit += [
          'field_paragraphs[' . $i . '][subform][field_images][0][alt]' => 'image',
        ];
        continue;
      }

      $edit += [
        'field_paragraphs[' . $i . '][subform][field_text][0][value]' => 'Some text',
      ];
    }
    $this->drupalPostForm(NULL, $edit, 'Save');
    $this->assertText('paragraphed_test 100MixedParagraphs has been created.');
    $new_stamp = $this->start('Saving paragraphs.');
    $this->timeStampsMixedParagraphs[key($new_stamp)] = reset($new_stamp);
    $this->drupalGet('node/1/edit');

    // Change edit mode to closed.
    $this->setParagraphsWidgetMode('paragraphed_test', 'field_paragraphs', 'closed');
    $new_stamp = $this->start('Changing mode to closed.');
    $this->timeStampsMixedParagraphs[key($new_stamp)] = reset($new_stamp);
    $this->drupalGet('node/1/edit');
    $this->drupalPostForm(NULL, NULL, 'Save');
    $new_stamp = $this->start('Saving node with closed mode.');
    $this->timeStampsMixedParagraphs[key($new_stamp)] = reset($new_stamp);

    // Change edit mode to preview.
    $this->setParagraphsWidgetSettings('paragraphed_test', 'field_paragraphs', ['closed_mode' => 'preview']);
    $new_stamp = $this->start('Changing mode to preview.');
    $this->timeStampsMixedParagraphs[key($new_stamp)] = reset($new_stamp);
    $this->drupalGet('node/1/edit');
    $this->drupalPostForm(NULL, NULL, 'Save');
    $new_stamp = $this->start('Saving node with preview mode.');
    $this->timeStampsMixedParagraphs[key($new_stamp)] = reset($new_stamp);
    $this->calculateSpentTime($this->timeStampsMixedParagraphs);
  }

  /**
   * Creating deep nested setup with only text paragraphs.
   */
  public function testCreateNestedParagraphs() {
    $this->addParagraphsType('text_type');
    static::fieldUIAddNewField('admin/structure/paragraphs_type/text_type', 'text', 'Text', 'text_long', [], []);
    // Add a nested Paragraph type.
    $paragraph_type = 'nested_paragraph';
    $this->addParagraphsType($paragraph_type);
    $this->drupalGet('node/add/paragraphed_test');
    static::fieldUIAddNewField('admin/structure/paragraphs_type/nested_paragraph', 'nested', 'Nested', 'field_ui:entity_reference_revisions:paragraph', [
      'settings[target_type]' => 'paragraph',
      'cardinality' => '-1',
    ], []);

    $this->drupalGet('node/add/paragraphed_test');
    $edit = ['title[0][value]' => 'NestedParagraphs'];
    $new_stamp = $this->start('Starting adding containers');
    $this->timeStampsNestedParagraphs[key($new_stamp)] = reset($new_stamp);
    for ($i = 0; $i < 10; $i++) {
      $this->drupalPostForm(NULL, NULL, 'Add nested_paragraph');
      for ($j = 0; $j < 3; $j++) {
        // Add nested paragrah.
        $this->drupalPostAjaxForm(NULL, NULL, 'field_paragraphs_' . $i . '_subform_field_nested_nested_paragraph_add_more');
        // Add 2 text paragraphs.
        $this->drupalPostAjaxForm(NULL, NULL, 'field_paragraphs_' . $i . '_subform_field_nested_' . $j . '_subform_field_nested_text_type_add_more');
        $this->drupalPostAjaxForm(NULL, NULL, 'field_paragraphs_' . $i . '_subform_field_nested_' . $j . '_subform_field_nested_text_type_add_more');
        $edit += [
          'field_paragraphs[0][subform][field_nested][' . $j . '][subform][field_nested][0][subform][field_text][0][value]' => 'Some text_1_' . $i . '_' . $j,
          'field_paragraphs[0][subform][field_nested][' . $j . '][subform][field_nested][1][subform][field_text][0][value]' => 'Some text_2_' . $i . '_' . $j,
        ];
      }
      $new_stamp = $this->start('Added paragraph container ' . $i . '.');
      $this->timeStampsNestedParagraphs[key($new_stamp)] = reset($new_stamp);
    }

    $this->drupalPostForm(NULL, $edit, 'Save');
    $this->assertText('paragraphed_test NestedParagraphs has been created.');
    $new_stamp = $this->start('Saving paragraphs.');
    $this->timeStampsMixedParagraphs[key($new_stamp)] = reset($new_stamp);

    // Change edit mode to closed.
    $this->setParagraphsWidgetMode('paragraphed_test', 'field_paragraphs', 'closed');
    $new_stamp = $this->start('Changing mode to closed.');
    $this->timeStampsNestedParagraphs[key($new_stamp)] = reset($new_stamp);
    $this->drupalGet('node/1/edit');
    $this->drupalPostForm(NULL, NULL, 'Save');
    $new_stamp = $this->start('Saving node with closed mode.');
    $this->timeStampsNestedParagraphs[key($new_stamp)] = reset($new_stamp);

    // Change edit mode to preview.
    $this->setParagraphsWidgetSettings('paragraphed_test', 'field_paragraphs', ['closed_mode' => 'preview']);
    $new_stamp = $this->start('Changing mode to preview.');
    $this->timeStampsNestedParagraphs[key($new_stamp)] = reset($new_stamp);
    $this->drupalGet('node/1/edit');
    $this->drupalPostForm(NULL, NULL, 'Save');
    $new_stamp = $this->start('Saving node with preview mode.');
    $this->timeStampsNestedParagraphs[key($new_stamp)] = reset($new_stamp);
    $this->calculateSpentTime($this->timeStampsNestedParagraphs);
  }

  /**
   * Recording time and connect with markup.
   *
   * @param string $markup
   *   Markup for which we want to record time.
   *
   * @return array
   *   Key value pair, time and markup.
   */
  public function start($markup) {
    $start = microtime(TRUE);
    $timeStamps[$markup] = $start;
    return $timeStamps;
  }

  /**
   * Calculate spent between every 2 actions.
   *
   * @param array $timeStamps
   *   Array with stored time stamps.
   */
  public function calculateSpentTime(array $timeStamps) {
    $diffTimeStamps[key($timeStamps)] = 0;
    foreach ($timeStamps as $marker => $time) {
      $next_value = next($timeStamps);
      if ($next_value) {
        $next_key = array_search($next_value, $timeStamps);
        $diffTimeStamps[$next_key] = round($next_value - $time, 3);
      }
    }

    $end_time = end($timeStamps);
    reset($timeStamps);
    $total_time = $end_time - $timeStamps[key($timeStamps)];
    $diffTimeStamps['Total time'] = round($total_time, 3);
    $this->outputTimeStamps($diffTimeStamps);
  }

  /**
   * Output time stamps in debug output.
   *
   * @param array $timeStamps
   *   Key value pair with markup and time.
   */
  public function outputTimeStamps(array $timeStamps) {
    foreach ($timeStamps as $marker => $time) {
      $this->assert(TRUE, $marker . ' ' . $time . ' sec', TRUE);
    }
  }

}
